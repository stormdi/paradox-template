$(document).ready(function(){
    // Сброс тестовых полей
    $('[type="text"]').val('');

    // Слайдер для фото
    $('.owl-carousel').each(function(){
        var $this = $(this),
            items = $this.data('items') || 3,
            marginRight = (function(){
                if($this.data('margin') == 0) {
                    return false;
                } else {
                    return $this.data('margin') || 10;
                }
            })(),
            isNav = (function(){
                if($this.data('nav') == '') {
                    return true;
                } else {
                    return false;
                }
            })();

      isDots = (function(){
        if($this.data('dots') == '') {
          return true;
        } else {
          return false;
        }
      })();

        //console.log(marginRight)
        if($this.data('couter') == '') {
            $this.on('initialized.owl.carousel translated.owl.carousel', function(e) {
                var $prevBtn = $this.find('.owl-prev'),
                    $counter = $prevBtn.next('.owl-count');

                if(e.type == 'initialized' || $counter.length == 0) {

                    $('<div class="owl-count" />')
                        .append('<span class="owl-count__index">'+ (e.item.index+1) +'</span>')
                        .append('<span class="owl-count__sep"/>')
                        .append('<span class="owl-count__total">'+ e.item.count+ '</span>')
                        .insertAfter($prevBtn);
                }

                if(e.type == 'translated') {
                    $counter.find('.owl-count__index').html(e.item.index+1)
                }
            })
        }
        $this.owlCarousel({
            items: items,
            dots: isDots,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            loop:true,
            // dotsContainer: ' ',
            nav: isNav,
            navText: ['‹','›'],
            margin: marginRight
        })
    });

    // Табы
    $('[data-tab]').click(function(){
        var $switherItem = $(this),
            tabId = "#" + $switherItem.data('tab'),
            activeClass = 'active';

        $switherItem
            .addClass(activeClass)
            .siblings()
            .removeClass(activeClass);

        $(tabId)
            .addClass(activeClass)
            .siblings()
            .removeClass(activeClass);
    });

    // Показать весь текст (о проекте)
    $('.js-expand-trigger').on('click', function(){
        var $this = $(this);
        $this.siblings('.expand-txt').addClass('expand-txt--expanded');
        $this.remove();
    });

    // Показать все фильтры
    $('.filter__grp-title').on('click', function(){
        $(this).closest('.filter__grp').toggleClass('filter__grp--active')
    });

    // Поиск
    var brands = new List('brands', {
        valueNames: [ 'brand-title']
    });

    // Ползунок значений
    $("[type='range']").ionRangeSlider({
        type: "double",
        min: 1000,
        max: 3500,
        step: 100,
        force_edges: true
    });

    // Кастомный селект
    $('select').selectric({
    placeholderOnOpen: false,
    optionsItemBuilder: function (itemData, element, index) {
      var
        $img = element.data('img') ? '<span class="select__img"><img src="' + element.data('img') + '" /></span>' : '',
        $text = '<span class="select__text">' + itemData.text + '</span>',
        $price = element.data('price') ? '<span class="select__price">' + element.data('price') + ' <span class="priceRu">руб.</span></span>' : '';
      return $img + $text + $price;
    }
  });

    //  Счетчк цифр
    $("input[type=number]").stepper({
        labels: {
            up: "+",
            down: "−"
        }
    });

    // Переключатель строк
    $('table').on('click', '.header', function(){
        var $this = $(this);
        $this.toggleClass('active');
        $.each($this.nextAll('tr'), function(i, el){
            var $el = $(el);
            if($el.hasClass('header')) {
                return false;
            }
            $el.toggleClass('opened');
        });
    });

    // Модальные окна
    $('[data-modal]').on('click', function(e){

        e.preventDefault();

        var $this = $(this),
            href = $this.attr('href'),
            type = 'html',
            url = '',
            content = '',
            openedClass = 'arcticmodal--opened',
            $body = $('body');

        if(href.charAt(0) == '#') {
            content = $(href);
        } else {
            type = 'ajax';
            url =  href;
        }

        $.arcticmodal({
            type: type,
            url: url,
            content: content,
            openEffect: {
                type: 'none',
                speed: 0
            },
            closeEffect: {
                type: 'none',
                speed: 0
            },
            overlay: {
                css: {
                    backgroundColor: false,
                    opacity: false
                }
            }
        });
    })

    // Фокус для поиска
    $('.header-base__search').on('click', '.js-search-trigger', function () {
        var $el = $(this),
            $parent = $el.closest('.header-base__search'),
            focusClass = 'header-base__search--focus';
        $parent.toggleClass(focusClass);
    });

    // Многострочное сокращение текста
    $(".txt-ellipsis").dotdotdot({
        watch: "window"
    });

    // Фиксатор для кнопки "Back to top"
    var $backToTop = $('.backToTop'),
        backToTopFixedClass = 'backToTop--fixed';

    $('.backToTop__inner').on('click', function () {
        window.scrollTo(0, 0);
    });

    var backToTopToggle = new Waypoint.Inview({
        element: $('.page-footer')[0],
        enter: function(direction) {
            if(direction === 'down') $backToTop.removeClass(backToTopFixedClass);
        },
        exited: function(direction) {
            if(direction == 'up') {
                $backToTop.addClass(backToTopFixedClass);
            }
        }
    });

    // Устанавливаем для поиска ширину по умолчанию, что бы анимация отрабатывала корректно
    $('.header-base__search').css('width', $('.header-base__search').outerWidth())

  var footerHeight = $('.page-footer').height();

  $(".checkout__cart").sticky({
    topSpacing:145,
    bottomSpacing: footerHeight + 20
    // wrapperClassName: 'checkout__col'
  });
});
