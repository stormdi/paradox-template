"use strict";
var gulp = require('gulp'),

	// Base
	concat = require('gulp-concat'),
	sourcemaps = require("gulp-sourcemaps"),
	rename = require('gulp-rename'),
	addSrc = require('gulp-add-src'),
	del = require('del'),

	//gutil = require('gulp-util'),

	// HTML
	jade = require('gulp-jade'),
	htmlReplace = require('gulp-html-replace'),
	htmlMinify = require('gulp-minify-html'),

	// CSS

	less = require('gulp-less'),
	autoprefixer = require('gulp-autoprefixer'),
	cssMinify = require('gulp-minify-css'),

	// JS
	modulizr = require('gulp-modulizr'),
	fixJs = require("gulp-fixmyjs"),
	streamify = require('gulp-streamify'),
	packer = require('gulp-packer'),

	// Img
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
	imageminPngquant = require('imagemin-pngquant'),

	// Ico
	svgSprite = require("gulp-svg-sprite"),

	// Font
	fontGen = require("gulp-fontgen"),

	// Zip
	//zip = require('gulp-zip'),

	// Server
	connect = require('gulp-connect');

var paths = {
	build:  'build',
	source: 'src',
	deploy: 'deploy/',
	html:   'src/jade/*.jade',
	style:  'src/less/main.less',
	images: 'src/img/**/*',
	ico: 	'src/ico/*',
    fonts:	'src/fonts/*.{ttf, otf}'
};
var watch = {
	html:   'src/jade/**/*',
	style:  'src/less/**/*',
	js:		'src/js/**/*',
	images: 'src/img/**/*'
};
var modernizrSrc = 'bower_components/nodernizr-dev/modernizr-latest.js',
	modernizrModules = [
		'html5shiv',
		'cssclasses',
		'svg',
		'flexbox',
		'multiplebgs'
	];
var jsLoad = [
	'bower_components/nodernizr-dev/modernizr-latest.js',
	'bower_components/svg-sprite-injector/svg-sprite-injector.js',
	'src/js/lib/font-loader.js',
	paths.source + '/js/load.js'
];
var jsMain = [
	'bower_components/jquery/dist/jquery.js',
	'bower_components/owl.carousel/dist/owl.carousel.js',
	'bower_components/ion.rangeslider/js/ion.rangeSlider.js',
	'bower_components/list.js/dist/list.js',
	'bower_components/jquery-selectric/public/jquery.selectric.js',
	'bower_components/jquery-selectric/public/plugins/jquery.selectric.placeholder.js',
	'bower_components/Stepper/jquery.fs.stepper.js',
	'bower_components/arcticmodalbw/arcticmodal/jquery.arcticmodal.js',
	'bower_components/jQuery.dotdotdot/src/jquery.dotdotdot.js',
	'bower_components/waypoints/lib/jquery.waypoints.js',
	'bower_components/waypoints/lib/shortcuts/inview.js',
	'bower_components/sticky/jquery.sticky.js',
	paths.source + '/js/main.js'
];

gulp.task('default', [
	//'connect',
	'build-html',
	'build-css',
	'build-fonts-css',
	'build-js',
	'build-img',
	'build-ico',
	'watcher'
]);
gulp.task('deploy', [
	'deploy-html',
	//'deploy-fonts-css',
	'deploy-css',
	'deploy-js',
	'deploy-img',
	'deploy-ico'
]);

// Fonts
gulp.task('build-fonts', function(){
	return gulp.src(paths.fonts)
		.pipe(fontGen({
			dest: paths.build + '/fonts/',
			css_fontpath: '../fonts/'
		}));
});
gulp.task('build-fonts-css', ['build-fonts'], function(){
	return gulp.src(paths.build + '/fonts/*.css')
			.pipe(concat('fonts.css'))
			.pipe(gulp.dest(paths.build + '/css/'));
});
gulp.task('deploy-fonts', function(){
	return gulp.src(paths.fonts)
		.pipe(fontGen({
			dest: paths.deploy + '/fonts/',
			css_fontpath: '/paradox/fonts/'
		}));
});
gulp.task('deploy-fonts-css', ['deploy-fonts'], function(){
	return gulp.src(paths.deploy + '/fonts/*.css')
			.pipe(concat('fonts.css'))
			.pipe(gulp.dest(paths.deploy + '/css/'));
});

// Jade
gulp.task('build-html', function() {
	gulp.src(paths.html)
		.pipe(jade({
			pretty: '\t'
		}))
		.pipe(gulp.dest(paths.build))
		//.pipe(connect.reload())
});

gulp.task('deploy-html', function() {
	gulp.src(paths.html)
		.pipe(jade())
		.pipe(htmlReplace({
			'fonts'		: 'css/fonts.min.css',
			'css'		: 'css/main.min.css',
			'js-load'	: 'js/load.min.js',
			'js-main'	: 'js/main.min.js'
		}))
		.pipe(htmlMinify())
		.pipe(gulp.dest(paths.deploy));
});

// Less
gulp.task('build-css', function(){
	gulp.src(paths.style)
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(autoprefixer())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.build + '/css'))
		//.pipe(connect.reload());
});

gulp.task('deploy-css', function(){
	gulp.src(paths.style)
		.pipe(less())
		.pipe(autoprefixer())
		.pipe(cssMinify())
		.pipe(rename('main.min.css'))
		.pipe(gulp.dest(paths.deploy + '/css'));
});

// Modernizr
gulp.task('build-modernizr', function() {
	return gulp.src(modernizrSrc)
		.pipe(modulizr(modernizrModules))
		.pipe(gulp.dest(paths.build +'/js/temp'));
});
gulp.task('deploy-modernizr', function() {
	return gulp.src(modernizrSrc)
		.pipe(modulizr(modernizrModules))
		.pipe(gulp.dest(paths.deploy +'/js/temp'));
});

// JS Load
gulp.task('build-js-load', function() {
	gulp.src(jsLoad)
		//.pipe(addSrc(paths.build +'/js/temp/*.js'))
		.pipe(sourcemaps.init())
		.pipe(concat('load.js'))
		.pipe(fixJs())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.build +'/js'))
		//.pipe(connect.reload());
	del([
		paths.build +'/js/temp/'
	])
});

gulp.task('deploy-js-load', function() {
	gulp.src(jsLoad)
		//.pipe(addSrc(paths.deploy +'/js/temp/*.js'))
		.pipe(concat('load.js'))
		.pipe(fixJs())
		.pipe(streamify(packer({
			base62: true,
			shrink: true
			})
		))
		.pipe(gulp.dest(paths.deploy +'/js'));
	del([
		paths.deploy +'/js/temp/'
	])
});

// JS Main
gulp.task('build-js', ['build-js-load'], function() {
	gulp.src(jsMain)
		.pipe(fixJs())
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.build +'/js'))
		//.pipe(connect.reload())
});
gulp.task('deploy-js', ['deploy-js-load'], function() {
	gulp.src(jsMain)
		.pipe(fixJs())
		.pipe(concat('main.js'))
		.pipe(streamify(packer({
			base62: true,
			shrink: true
		})))
		.pipe(gulp.dest(paths.deploy +'/js'));
});

// Img
gulp.task('build-img', function(){
	gulp.src(paths.images + '.jpg')
		.pipe(imageminJpegRecompress({loops: 3})())
		.pipe(gulp.dest(paths.build + '/img'))
		.pipe(connect.reload())
	gulp.src(paths.images + '.png')
		.pipe(imageminPngquant({
			quality: '65-80',
			speed: 1
		})())
		.pipe(gulp.dest(paths.build + '/img'))
		//.pipe(connect.reload())
});

gulp.task('deploy-img', function(){
	gulp.src(paths.images + '.jpg')
		.pipe(imageminJpegRecompress({loops: 3})())
		.pipe(gulp.dest(paths.deploy+ '/img'));
	gulp.src(paths.images + '.png')
		.pipe(imageminPngquant({
			quality: '65-80',
			speed: 1
		})())
		.pipe(gulp.dest(paths.deploy + '/img'))
});

// Ico
gulp.task('build-ico', function () {
	return gulp.src(paths.ico)
		.pipe(svgSprite({
			mode: {
				symbol: true
			},
			shape: {
				transform: [{
					svgo: {
						plugins: [
							{convertShapeToPath: true},
							{convertPathData: true},
							{mergePaths: true},
							{convertTransform: true},
							{removeUnusedNS: true},
							{cleanupIDs: true},
							{cleanupNumericValues: true},
							{removeUselessStrokeAndFill: true},
							{removeHiddenElems: true},
							{removeDoctype: false}
						]
					}
				}]
			}
		}))
		.pipe(rename('ico/sprite.svg'))
		.pipe(gulp.dest(paths.build))
		//.pipe(connect.reload())
});

gulp.task('deploy-ico', function () {
	return gulp.src(paths.ico)
		.pipe(svgSprite({
			mode: {
				symbol: true
			},
			shape: {
				transform: [{
					svgo: {
						plugins: [
							{convertShapeToPath: true},
							{convertPathData: true},
							{mergePaths: true},
							{convertTransform: true},
							{removeUnusedNS: true},
							{cleanupIDs: true},
							{cleanupNumericValues: true},
							{removeUselessStrokeAndFill: true},
							{removeHiddenElems: true},
							{removeDoctype: false}
						]
					}
				}]
			}
		}))
		.pipe(rename('ico/sprite.svg'))
		.pipe(gulp.dest(paths.deploy))
});

// ZIP
gulp.task('zip', function(){
	gulp.src(paths.deploy + '**/*')
		.pipe(zip('html.zip'))
		.pipe(gulp.dest(paths.deploy));
});

// server connect
gulp.task('connect', function(){
	connect.server({
		root: paths.build,
		//livereload: true
	})
});

// Watch
gulp.task('watcher', function(){
	gulp.watch(watch.html,  ['build-html']);
	gulp.watch(watch.style, ['build-css']);
	gulp.watch(watch.js,    ['build-js']);
	gulp.watch(watch.images,['build-img']);
	gulp.watch(watch.ico,	['build-ico']);
});
